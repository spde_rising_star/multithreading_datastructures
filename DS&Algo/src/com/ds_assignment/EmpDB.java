package com.ds_assignment;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class EmpDB 
{
	List<Emp> employeeDB = new ArrayList<>();
	public boolean addEmployee(Emp e)
	{
		return employeeDB.add(e);
	}
	public boolean deleteEmployee(int empId)
	{
		boolean isRemoved = false;
		
		Iterator<Emp> it = employeeDB.iterator();
		
		while(it.hasNext())
		{
			Emp emp=it.next();
			if(emp.getEmpId()==empId)
			{
				isRemoved=true;
				it.remove();
			}
		}
		return isRemoved;
	}
	public String showPaySlip(int EmpId)
	{
		String paySlip = "Invalid employee id";
		for(Emp e: employeeDB)
		{
			if(e.getEmpId()==EmpId)
			{
				paySlip = "Payslip for employee id" + EmpId + "is " + e.getSalary();
				
			}
		}
		return paySlip;
	}
	public Emp[] listAll()
	{
		Emp[] empArray= new Emp[employeeDB.size()];
		for(int i=0; i<employeeDB.size();i++)
			empArray[i]=employeeDB.get(i);
		return empArray;
		
		
	}

}