package com.multithread_assignment;

class MyFirstThread implements Runnable{
	
	synchronized public void run() {
		try {
			int i=0;
			while(i<5){
				Thread.sleep(1000);
				System.out.println("Good Morning");
				i++;
			}
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
}
class MySecondThread implements Runnable{
	
	synchronized public void run() {
		try {
			int i=0;
			while(i<5){
				java.lang.Thread.sleep(2000);
				System.out.println("Hello");
				i++;
			}
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
}
class MyThirdThread implements Runnable{
	
	synchronized public void run() {
		try {
			int i=0;
			while(i<5){
				java.lang.Thread.sleep(3000);
				System.out.println("EveryOne");
				i++;
			}
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
}

public class MultiThreadExp2 {
   public static void main(String[] as) {
	 MyFirstThread t1=new MyFirstThread();
	 Thread mt1=new  Thread(t1);
	 MySecondThread t2=new MySecondThread();
	 Thread mt2=new  Thread(t2);
	 MyThirdThread t3=new MyThirdThread();
	 Thread mt3=new  Thread(t3);
	 mt1.start();
	 mt2.start();
	 mt3.start();
	 
	 
   
}
}
