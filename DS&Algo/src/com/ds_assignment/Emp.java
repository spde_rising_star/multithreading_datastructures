package com.ds_assignment;
public class Emp
{
	public int EmpId;
	public String EmpName;
	public String Email;
	public char Gender;
	public float Salary;
	
	public Emp()
	{
		
	}
		public Emp(int empId, String empName, String email, char gender, float salary)
		{
			super();
			EmpId = empId;
			EmpName = empName;
			Email = email;
			Gender = gender;
			Salary = salary;
		}
		public String GetEmployeeDetails()
		{
			return "Emp [EmpId=" + EmpId + ",EmpName = " + EmpName + ",Email =" + Email +",Gender=" + Gender + ",Salary=" + Salary +"]"; 
		}
		public int getEmpId()
		{
			return EmpId;
		}
		public void setEmpId(int empId)
		{
			EmpId = empId;
		}
		public String getEmpName()
		{
			return EmpName;
		}
		public void setEmpName(String empName)
		{
			EmpName = empName;
		}
		public String getEmail()
		{
			return Email;
		}
		public void setEmail(String email)
		{
			Email = email;
		}
		public char getGender()
		{
			return Gender;
		}
		public void setGender(char gender)
		{
			Gender = gender;
		}
		public float getSalary()
		{
			return Salary;
		}
}