package com.ds_assignment;
public class EmpTest 
{
	public static void main(String[] args)
	{
		EmpDB empDB = new EmpDB();
		Emp e1 = new Emp(011,"Aamer","ammu@gmail.com",'m',25000);
		Emp e2 = new Emp(022,"younus","younus@gmail.com",'m',28000);
		Emp e3 = new Emp(033,"waseem","waseem@gmail.com",'m',31000);
		
		empDB.addEmployee(e1);
		empDB.addEmployee(e2);
		empDB.addEmployee(e3);
		
		for(Emp emp : empDB.listAll())
			System.out.println(emp.GetEmployeeDetails());
		System.out.println();
		empDB.deleteEmployee(022);
		
		for(Emp emp : empDB.listAll())
			System.out.println(emp.GetEmployeeDetails());
		
		System.out.println();
		System.out.println(empDB.showPaySlip(3));
	}

}