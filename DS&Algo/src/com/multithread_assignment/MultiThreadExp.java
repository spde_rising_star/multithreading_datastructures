package com.multithread_assignment;

class FirstThread extends Thread{
	
	synchronized public void run() {
		try {
			int i=0;
			while(i<5){
				sleep(1000);
				System.out.println("Good Morning");
				i++;
			}
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
}
class SecondThread extends Thread{
	
	synchronized public void run() {
		try {
			int i=0;
			while(i<5){
				sleep(2000);
				System.out.println("Hello");
				i++;
			}
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
}
class ThirdThread extends Thread{
	
	synchronized public void run() {
		try {
			int i=0;
			while(i<5){
				sleep(3000);
				System.out.println("EveryOne");
				i++;
			}
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
}

public class MultiThreadExp {
   public static void main(String[] as) {
	 FirstThread t1=new FirstThread();
	 SecondThread t2=new SecondThread();
	 ThirdThread t3=new ThirdThread();
	 t1.start();
	 t2.start();
	 t3.start();
	 
	 
	
}
}
