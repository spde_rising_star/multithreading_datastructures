package com.ds_assignment;

import java.util.Iterator;
import java.util.LinkedList;

public class EvenNoLinkedList {
	LinkedList<Integer> a1 = new LinkedList<Integer>();
	LinkedList<Integer> a2 = new LinkedList<Integer>();

	public void saveEvenNumbers(int n) {
		for (int i = 1; i <= n; i++) {
			if (i % 2 == 0) {
				a1.add(i);
			}
		}
	}

	public LinkedList<Integer> saveEvenNumbers() {
		return a1;
	}

	public LinkedList<Integer> printEvenNumbers() {
		for (int i = 0; i < a1.size(); i++) {
			int b = (a1.get(i)) * 2;
			a2.add(b);
		}
		Iterator<Integer> itr = a2.iterator();
		while (itr.hasNext()) {
			System.out.print(itr.next() + "\t");
		}
		System.out.println();
		return a2;
	}

	public int printEvenNumber(int n) {
		int r = 0;
		for (int i = 0; i < a2.size(); i++) {
			if (n == a2.get(i)) {
				r = n;
			}

		}
		return r;

	}

	public static void main(String[] as) {
		EvenNoLinkedList obj = new EvenNoLinkedList();
		obj.saveEvenNumbers(10);
		System.out.println(obj.saveEvenNumbers());
		obj.printEvenNumbers();
		System.out.println(obj.printEvenNumber(20));

	}
}