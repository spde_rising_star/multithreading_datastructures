package com.multithread_assignment;
public class SyncronizationSecondThreadClass extends Thread
{
	SynchronizationClass s;
	SyncronizationSecondThreadClass(SynchronizationClass s)
	{
		this.s=s;
	}
	public void run(int b,String a)
	{
		try 
		{
			Thread.sleep(b);
			s.odd();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		System.out.println();
		System.out.println(a);
	}
}