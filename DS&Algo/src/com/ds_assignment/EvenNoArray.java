package com.ds_assignment;

import java.util.ArrayList;
import java.util.Iterator;

public class EvenNoArray {
	ArrayList<Integer> a1 = new ArrayList<Integer>();
	ArrayList<Integer> a2 = new ArrayList<Integer>();

	public void saveEvenNumbers(int n) {
		for (int i = 1; i <= n; i++) {
			if (i % 2 == 0) {
				a1.add(i);
			}
		}
	}

	public ArrayList<Integer> saveEvenNumbers() {
		return a1;
	}

	public ArrayList<Integer> printEvenNumbers() {
		for (int i = 0; i < a1.size(); i++) {
			int b = (a1.get(i)) * 2;
			a2.add(b);
		}
		Iterator<Integer> itr = a2.iterator();
		while (itr.hasNext()) {
			System.out.print(itr.next() + "\t");
		}
		System.out.println();
		return a2;
	}

	public int printEvenNumber(int n) {
		int r = 0;
		for (int i = 0; i < a2.size(); i++) {
			if (n == a2.get(i)) {
				r = n;
			}

		}
		return r;

	}

	public static void main(String[] as) {
		EvenNoArray obj = new EvenNoArray();
		obj.saveEvenNumbers(10);
		System.out.println(obj.saveEvenNumbers());
		obj.printEvenNumbers();
		System.out.println(obj.printEvenNumber(8));

	}
}